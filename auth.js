const jwt = require("jsonwebtoken")
const secret = "IDoNotKnow" // form of message / string in which this will serve as our secre "code"````

// JSON webtoken - is a way of securely passing of information from oa part of server to the front end.
// info is kept secure through the use of secret variable
// secret variable is only known by the system/browser which can be decoded by browser

// token creation
module.exports.createAccessToken = (user) => {
    // data will be received from the registration form, when the user login, a token will be created withj the user's information (this information still encrypted inside the token)
    const data = {
        id : user._id,
        email: user.email,
        isAdmin: user.isAdmin
    }

    // generates the token using the form data and the secret code without additional options
    return jwt.sign(data, secret, {})
}
// token verification 
module.exports.verify = (req, res, next) => {
    let token = req.headers.authorization // authorization can be found in the headers of th erequest and tells whether the client/user has the autority to send the request.

    if (typeof token !== "undefined") {//token is present
        console.log(token)
        token = token.slice(7, token.length)


        // jwt.verify - ferifies the thoken using the secret, and fails if the token's secret does not matcgh the secret variable,meaning there is an attempt hack, or at least to tamper/change the daya from the user=end

        return jwt.verify(token,secret,(err,data) => {
            if(err){
                return res.send({auth: "failed"})
            } else {
                // tells the server to proced to processing of the request
                next();
            }
        })
    }
}

// token decoding 

module.exports.decode = (token) => {
    if (typeof token !== "undefined"){
        token = token.slice(7, token.length)
        return jwt.verify(token, secret, (err,data) => {
            if (err) {
                console.log(err)
                return null 
            } else {
                return jwt.decode(token, {complete: true}).payload
                // jwt.decode - decides the data to be decoded, which is the toke,
                // payload - the one that we need ot verify the user informatio this is a par tof the token when the code the createAccessTOken function(the on with the _id, email, and is amdin)
            }
        })      
    } else {
        return null // there is no token
    }
}