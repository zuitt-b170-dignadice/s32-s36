/* 
import the necessary models that you will need to perform CRUD operations
*/
const User = require("../models/user.js")
const Course = require("../models/course.js")

/* 

Activity s34
    1. Find the user in the database 
        find out if the user id admin
    
    2. If the user is not an admin, send the response "You are not an admin"

    3. If the user is an admin, create the new Course object
        - save the object 
            - if there are errors, return false
            - if there are no errors, return "course created successfully"

*/

module.exports.addCourse = (reqBody, userData) => {
    return User.findById(userData.userId).then(result => {
        if (userData.isAdmin ==  false) {
            return "You are not an admin"
        } else {
            let newCourse = new Course({
                name: reqBody.name,
                description : reqBody.description,
                price: reqBody.price
            })
            return newCourse.save().then((course,error) => {
                if(error){
                    return false
                } else {
                    return "course creation successful"
                }
            })
        
        }
    })
}